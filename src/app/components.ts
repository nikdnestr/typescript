export const kinoContainer = document.getElementById('film-container')!;
export const favoriteKinoContainer =
    document.getElementById('favorite-movies')!;
export const randomKinoContainer = document.getElementById(
    'random-movie-background'
);
export const randomKinoName = document.getElementById('random-movie-name');
export const randomKinoDescription = document.getElementById(
    'random-movie-description'
);
export const searchForm = document.getElementById('search') as HTMLInputElement;

export const popularButton = document.getElementById('popular');
export const upcomingButton = document.getElementById('upcoming');
export const topRatedButton = document.getElementById('top_rated');
export const submitButton = document.getElementById('submit');
export const loadMoreButton = document.getElementById('load-more');

export const makeCard = (
    id: string,
    overview: string,
    poster: string,
    release: string,
    isLiked: boolean,
    isFavorite: boolean
) => {
    let src = `https://image.tmdb.org/t/p/w500${poster}`;
    if(poster === null) {
        src =
            'https://images-na.ssl-images-amazon.com/images/I/41bLP6NzvKL.jpg';
    }
    const card = `<div class="${
        isFavorite ? 'col-12 p-2' : 'col-lg-3 col-md-4 col-12 p-2'
    }">
                <div class="card shadow-sm">
                    <img
                        class="main-poster"
                        src="${src}"
                    />
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            id="${id}"
                            stroke="red"
                            fill="${isLiked ? 'red' : '#ff000078'}"
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                fill-rule="evenodd"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
                            <div class="card-body">
                                <p class="card-text truncate main-description">${overview}</p>
                                    <div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                    <small class="text-muted main-release-date">${release}</small>
                                </div>
                            </div>
                </div>
            </div>`;
    return card;
};

import { api } from '../api/api';

import { kinoContainer } from './components';

import { showRequestedKino, showRandomKino } from './renderer';

export interface Kino {
    id: string;
    title: string;
    overview: string;
    poster: string;
    release: string;
    backdrop: string;
}

export const objectMapper = (response: any): Kino[] => {
    return response.map((kino: any) => {
        return {
            id: kino.id,
            title: kino.title,
            overview: kino.overview,
            release: kino.release_date,
            poster: kino.poster_path,
            backdrop: kino.backdrop_path,
        };
    });
};

export const makeApiCall = async (request: string, query?: string) => {
    const res = await api(request, query);
    return res.data.results;
};

export const initializeHearts = (hearts: NodeListOf<Element>) => {
    for (let i = 0; i < hearts.length; i++) {
        let heart = hearts[i];
        heart.addEventListener('click', async () => {
            if (localStorage.getItem(heart.id) === null) {
                heart.setAttribute('fill', 'red');
                localStorage.setItem(heart.id, heart.id);
            } else {
                heart.setAttribute('fill', '#ff000078');
                localStorage.removeItem(heart.id);
            }
        });
    }
};

export const initializeCategoryButton = (
    button: HTMLElement,
    newCategory: string
) => {
    button.onclick = async () => {
        kinoContainer.textContent = '';
        localStorage.setItem('category', newCategory);
        const response = await makeApiCall(`movie/${newCategory}`);
        const kinoList = objectMapper(response);
        showRandomKino(kinoList);
        showRequestedKino(kinoList);
    };
};

export const getLikedKino = async () => {
    const storage = Object.keys(localStorage);
    const kino = [];
    for (let i = 0; i < storage.length; i++) {
        if (storage[i] !== 'category') {
            let item = await api(`movie/${storage[i]}`);
            console.log(item.data);
            kino.push(item.data);
        }
    }
    return objectMapper(kino);
};

export const checkForId = (id: string) => {
    let isLiked = false;
    const storageKeys = Object.keys(localStorage);
    for (let i = 0; i < storageKeys.length; i++) {
        if (id.toString() === storageKeys[i]) {
            isLiked = true;
        }
    }
    return isLiked;
};

import { makeApiCall, objectMapper } from './app/helper';

import { initialRender } from './app/renderer';

export async function render(): Promise<void> {
    let page = 1;
    const initialContent = await makeApiCall('movie/popular');
    const kinoList = objectMapper(initialContent);

    initialRender(kinoList, page);
}

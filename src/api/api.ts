import axios from 'axios';

export const api = (request: string, additionalQuery: string = "") => {
    const API_KEY = '369e9cf556488fd1117796dffad05547';
    const response = axios.get(
        `https://api.themoviedb.org/3/${request}?api_key=${API_KEY}${additionalQuery}`
    );
    return response;
};

import {
    kinoContainer,
    favoriteKinoContainer,
    makeCard,
    randomKinoContainer,
    randomKinoName,
    randomKinoDescription,
    searchForm,
    popularButton,
    upcomingButton,
    topRatedButton,
    submitButton,
    loadMoreButton,
} from './components';

import {
    Kino,
    checkForId,
    initializeHearts,
    objectMapper,
    makeApiCall,
    getLikedKino,
    initializeCategoryButton,
} from './helper';

export const initialRender = async (initialKinoList: Kino[], page: number) => {
    localStorage.setItem('category', 'popular');
    showRandomKino(initialKinoList);
    showRequestedKino(initialKinoList);
    await showFavoriteKino();
    if (
        popularButton &&
        upcomingButton &&
        topRatedButton &&
        loadMoreButton &&
        submitButton &&
        searchForm
    ) {
        initializeCategoryButton(popularButton, 'popular');
        initializeCategoryButton(upcomingButton, 'upcoming');
        initializeCategoryButton(topRatedButton, 'top_rated');
        showSearchedKino(submitButton, searchForm);
        showNextPage(loadMoreButton, page);
    }
};

export const showRequestedKino = (
    kinoList: Kino[],
    container: HTMLElement = kinoContainer
): void => {
    for (let i = 0; i < kinoList.length; i++) {
        let isLiked = checkForId(kinoList[i].id);
        let isFavorite = false;
        if (container !== kinoContainer) {
            isFavorite = true;
        }
        const { id, overview, poster, release } = kinoList[i];

        let card = makeCard(id, overview, poster, release, isLiked, isFavorite);
        container?.insertAdjacentHTML('beforeend', card);
    }
    const hearts = document.querySelectorAll('.bi-heart-fill');
    initializeHearts(hearts);
};

export const showRandomKino = (kinoList: Kino[]): void => {
    const randomNumber = Math.floor(Math.random() * kinoList.length);
    const { title, overview, backdrop } = kinoList[randomNumber];

    if (randomKinoName && randomKinoDescription && randomKinoContainer) {
        randomKinoName.textContent = title;
        randomKinoDescription.textContent = overview;
        if (backdrop) {
            randomKinoContainer.style.cssText = `
                background-image: url(https://image.tmdb.org/t/p/w500${backdrop});
                background-size:100% auto;
                background-repeat:no-repeat;
            `;
        }
    }
};

export const showSearchedKino = (
    button: HTMLElement,
    form: HTMLInputElement
) => {
    button.onclick = async () => {
        kinoContainer.textContent = '';
        const input = encodeURI(form.value);
        const response = await makeApiCall(`search/movie`, `&query=${input}`);
        showRequestedKino(objectMapper(response));
        form.value = '';
    };
};

export const showFavoriteKino = async () => {
    favoriteKinoContainer.innerText = '';
    showRequestedKino(await getLikedKino(), favoriteKinoContainer);
};

export const showNextPage = (button: HTMLElement, page: number) => {
    button.onclick = async () => {
        page++;
        let category;
        if (localStorage.getItem('category')) {
            category = localStorage.getItem('category');
        }
        const response = await makeApiCall(
            `movie/${category}`,
            `&page=${page}`
        );
        const kinoList = objectMapper(response);
        showRequestedKino(kinoList);
    };
};
